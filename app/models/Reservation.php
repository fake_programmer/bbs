<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $guarded=[];

    public function user(){

        return $this->hasOne(User::class, 'id','user_id');
    }
    public function booking(){

        return $this->hasOne(Booking::class, 'id','booking_id');
    }
    public function coach(){

        return $this->hasOne(Bus::class, 'coach_id','coach_id');
    }
}
