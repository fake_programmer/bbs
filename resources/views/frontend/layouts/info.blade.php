
@extends ('frontend.master')

@section('content')
<style>
  .trip-list {
    color: #333;
  }
  .single-trip {
    display: flex;
    border: 1px solid #c5c5c5;
    align-items: center;
  }

  .single-trip {
    margin-bottom: 15px;
  }
  .trip-section {
    padding: 10px;
    flex: 1;
  }

span.st_num {
    padding: 2px;
    background: #87d60b;
    margin-left: 4px;
    color: red;
}
</style>
<br>

<section>
<div class="container">

<div class="trip-list">
  <div class="single-trip">
    <div class="trip_info d-flex">
      <div class="trip-section">
        Name:  {{auth()->user()->name}}
      </div>
      <div class="trip-section">
        Coach Id::  {{$buses->coach_id}}
      </div>

      <div class="trip-section">From:: {{$buses->routelocation->from}}
      </div>
      <div class="trip-section">To:: {{$buses->routelocation->to}}
      </div>
      <div class="trip-section">Reporting:: {{$buses->reporting}}
      </div>
      <div class="trip-section">Depature:: {{$buses->depature}}
      </div>
      <div class="trip-section">Boarding:: {{$buses->boarding}}
      </div>
      <div class="trip-section">Seat Number:: 
        @foreach($seatNumber as $data)
          <span class="st_num">
            {{$data}}
          </span>
        @endforeach
      </div>
      <div class="trip-section">
        Unit Price::  {{$buses->price}}
      </div>
      <div class="trip-section">
        Total Price:: {{$totalPrice}}
      </div>
    </div>
    <div class="trip_amm">
      <form action="{{route('ticket')}}" method="post" role="form">   
      @csrf     
        <div class="form-group">
          <label for="bkash">Bkash (01711-711811)</label>
          <input type="text" class="form-control" id="bkash" name="bkash" placeholder="Enter bkash transaction id" required>
        </div>
        <div class="trip-section">          
          <button type="submit" class="btn btn-success">Confirm Booking</button>
        </div>
      </form>
    </div>
  </div>
</div>



</div>
</section><br>
@stop
