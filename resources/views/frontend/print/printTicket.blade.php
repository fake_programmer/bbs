@extends ('frontend.master')

@section('content')
    <section>
        <div class="container">
            <h1>Booking Details</h1>
            <br><br>

            <div class="table-responsive" >
                <table class="table table-striped" id="orderTable">
                    <thead>
                    <tr>
                        <th>Serial</th>
                        <th>From</th>
                        <th>From</th>
                        <th>Coach Number</th>
                        <th>Unit Price</th>
                        <th>Seat</th>
                        <th>Quantity</th>
                        <th>Total Amount</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $key=>$bus)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$bus->coach->routelocation->from}}</td>
                            <td>{{$bus->coach->routelocation->to}}</td>
                            <td>{{$bus->coach_id}}</td>
                            <td>{{$bus->booking->unit_price}}</td>
                            <td>{{$bus->seat}}</td>
                            <td>{{$bus->booking->quantity}}</td>
                            <td>{{$bus->booking->total_amount}}</td>
                            <td>
                                <a href="{{route('downloadPDF', $bus->id)}}">Print</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>

@stop
