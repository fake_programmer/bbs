<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>
<table class="table table-bordered">
    <tr>
        <td>Name: {{$downloadPdf->user->name}}</td>
        <td>Phone: {{$downloadPdf->user->contact_num}}</td>
    </tr>
    <tr>
        <td>From: {{$downloadPdf->coach->routelocation->from}}</td>
        <td>To: {{$downloadPdf->coach->routelocation->to}}</td>
    </tr>
    <tr>
        <td>Coach Number: {{$downloadPdf->coach_id}}</td>
        <td>Price: {{$downloadPdf->booking->unit_price}}</td>
    </tr>
    <tr>
        <td>Seat No: {{$downloadPdf->seat}}</td>
    </tr>
    <tr>
        <td>Quantity: {{$downloadPdf->booking->quantity}}</td>
        <td>Total: {{$downloadPdf->booking->total_amount}}</td>
    </tr>
</table>
</body>
</html>
