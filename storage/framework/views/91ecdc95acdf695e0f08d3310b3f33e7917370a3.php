<style>
  .trip-list {
    color: #333;
  }
  .single-trip {
    display: flex;
    border: 1px solid #c5c5c5;
    align-items: center;
  }

  .single-trip {
    margin-bottom: 15px;
  }
  .trip-section {
    padding: 10px;
    flex: 1;
  }
</style>
<?php $__env->startSection('content'); ?>
<br>

<section>
<div class="container">
<?php $__currentLoopData = $buses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$bus): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>


<div class="trip-list">
  <div class="single-trip">
    <div class="trip-section">
      Coach Id:  <?php echo e($bus->coach_id); ?>

    </div>
    <div class="trip-section">From:: <?php echo e($bus->routelocation->from); ?>

    </div>
    <div class="trip-section">To:: <?php echo e($bus->routelocation->to); ?>

    </div>
    <div class="trip-section">Reporting:: <?php echo e($bus->reporting); ?>

    </div>
    <div class="trip-section">Depature:: <?php echo e($bus->depature); ?>

    </div>
    <div class="trip-section">Boarding:: <?php echo e($bus->boarding); ?>

    </div>
    <div class="trip-section">Price:: <?php echo e($bus->price); ?>

    </div>
    <div class="trip-section">
      <a href="<?php echo e(route('showseat',$bus->id)); ?>" class="btn btn-primary">Available Seats</a>
    </div>
  </div>
</div>


<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>
</section><br>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\booking-system\resources\views/frontend/layouts/show.blade.php ENDPATH**/ ?>